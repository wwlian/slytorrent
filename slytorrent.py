#!/usr/bin/env python

import binascii
import glob
from optparse import OptionParser
import os
import random
import socket
import sys
import threading
import time
import urllib

import torrentfile_util
import tracker_util

def main():
  random.seed()
  arg_dict = parse_args()
  if (not arg_dict['torrents']) and (not arg_dict['download_torrents']):
    sys.exit('No torrent files found.')
  overall_download_rate_kbps = arg_dict['overall_download_rate_kbps']
  individual_download_rate_kbps = arg_dict['individual_download_rate_kbps']
  st = SlyTorrent(arg_dict['torrents'], arg_dict['overall_upload_rate_kbps'],
                  arg_dict['individual_upload_rate_kbps'],
                  download_torrents=arg_dict['download_torrents'],
                  overall_download_rate_kbps=overall_download_rate_kbps,
                  individual_download_rate_kbps=individual_download_rate_kbps)
  try:
    st.run()
  except KeyboardInterrupt:
    st.stop()

def parse_args():
  parser = OptionParser()
  parser.add_option('-t', '--torrent', dest='torrent_files', action='append', 
                    help='The path to a torrent file to load.  May be specified'
                    ' multiple times.', default=[])
  parser.add_option('-d', '--torrent-dir', dest='nonrecursive_torrent_dirs', 
                    action='append', help='Load torrent files found in the '
                    'top-level of the specified directory path. May be '
                    'specified multiple times.', default=[])
  parser.add_option('-r', '--recursive-dir', dest='recursive_torrent_dirs',
                    action='append', help='Load torrent files found in the '
                    'specified directory path, searching recursively in '
                    'subdirectories. May be specified multiple times.',
                    default=[])
  parser.add_option('--overall-upload-rate-kbps', 
                    dest='overall_upload_rate_kbps', type='int', default=1500, 
                    help='Set the maximum upload rate across all torrents in '
                    'KB/s.  Defaults to 1500 KB/s.')
  parser.add_option('--individual-upload-rate-kbps', 
                    dest='individual_upload_rate_kbps', type='int', default=250, 
                    help='Set the maximum upload rate per torrent in KB/s. '
                    'Defaults to 250 KB/s.')
  parser.add_option('--download-torrent', dest='download_torrents',
                    action='append', help='The path to a torrent file to '
                    'fake download prior to fake uploading. May be specified '
                    'multiple times.', default=[])
  parser.add_option('--download-dir', dest='nonrecursive_download_dirs',
                    action='append', help='Load torrent files for fake '
                    'downloading from the top-level of the specified directory.'
                    'May be specified multiple times.', default=[])
  parser.add_option('--recursive-download-dir', dest='recursive_download_dirs',
                    action='append', help='Load torrent files for fake download'
                    ' from the specified directory path, searching recursively '
                    'in subdirectories. May be specified multiple times.',
                    default=[])
  parser.add_option('--overall-download-rate-kbps', 
                    dest='overall_download_rate_kbps', type='int', default=5000,
                    help='Set the maximum download rate across all torrents in '
                    'KB/s.  Defaults to 5000 KB/s.')
  parser.add_option('--individual-download-rate-kbps', 
                    dest='individual_download_rate_kbps', type='int', 
                    default=1000, help='Set the maximum upload rate per torrent'
                    ' in KB/s. Defaults to 1000 KB/s.')
  options, args = parser.parse_args()
  results = {}
  torrents = []
  download_torrents = []
  results['overall_upload_rate_kbps'] = options.overall_upload_rate_kbps
  results['individual_upload_rate_kbps'] = options.individual_upload_rate_kbps
  results['overall_download_rate_kbps'] = options.overall_download_rate_kbps
  results['individual_download_rate_kbps'] = options.individual_download_rate_kbps

  # Validate -t/--torrent arguments.
  for t in options.torrent_files:
    if not os.path.isfile(t):
      raise IOError('Specified torrent file does not exist: ' + t)
    torrents.append(t)

  # Validate -d/--torrent-dir arguments and collect torrent files.
  for d in options.nonrecursive_torrent_dirs:
    torrents.extend(find_torrents(d, False))

  # Validate -r/--recursive-dir arguments and collect torrent files.
  for d in options.recursive_torrent_dirs:
    torrents.extend(find_torrents(d, True))

  # Validate --download-torrent arguments.
  for t in options.download_torrents:
    if not os.path.isfile(t):
      raise IOError('Specified torrent file does not exist: ' + t)
    download_torrents.append(t)

  # Validate --download-dir arguments and collect torrent files.
  for d in options.nonrecursive_download_dirs:
    download_torrents.extend(find_torrents(d, False))

  # Validate --recursive-download-dir arguments and collect torrent files.
  # Validate -r/--recursive-dir arguments and collect torrent files.
  for d in options.recursive_download_dirs:
    download_torrents.extend(find_torrents(d, True))

  results['torrents'] = torrents
  results['download_torrents'] = download_torrents

  return results
  
def find_torrents(dir_path, recursive=False):
  if not os.path.isdir(d):
    raise IOError('Specified directory does not exist: ' + d)
  torrents = []
  if recursive:
    for dirpath, dirs, files in r:
      for f in files:
        if f.endswith('.torrent'):
          torrents.append(os.path.join(dirpath, f))
  else:
    for t in glob.iglob('*.torrent'):
      if os.path.isfile(t):
        torrents.append(t)
  return torrents

class SlyTorrent(object):
  UPDATE_INTERVAL_SEC = 60
  def __init__(self, torrents, overall_upload_rate_kbps, 
               individual_upload_rate_kbps, peer_port=0, download_torrents=[],
               overall_download_rate_kbps=0, indnividual_download_rate_kbps=0):
    self.torrents = torrents
    self.download_torrents = download_torrents
    self.overall_upload_rate_kbps = overall_upload_rate_kbps
    self.individual_upload_rate_kbps = individual_upload_rate_kbps
    self.overall_download_rate_kbps = overall_download_rate_kbps
    self.individual_download_rate_kbps = individual_download_rate_kbps

    self.torrents_info = {} # Map torrent infohash to SlyTorrentMetainfo
    # Map torrent infohash ot SlyTorrentMetainfo
    self.download_torrents_info = {} 

    # Map infohash to Lock object.  Prevents querying tracker in the middle
    # of updating the upload and download amounts.
    # TODO: Handle torrent duplicated in up and down queues.
    self.torrent_locks = {}

    # TODO: use uTorrent Mac peer_id version instead
    self.peer_id = '-TR2750-' + os.urandom(12)
    self.key = binascii.hexlify(os.urandom(4)).upper()
    self.listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                                       socket.IPPROTO_TCP)
    self.listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.listen_socket.bind(('', peer_port))
    # self.listen(5)
    self.peer_port = self.listen_socket.getsockname()[1]

    # Map torrent infohash to Timer obj for querying tracker
    self.tracker_timers = {} 
    self.tracker_timers_lock = threading.Lock()
    self.running = False
    self.timers_updating = 0
    self.timers_updating_lock = threading.Lock()
    self.none_updating = threading.Condition()

    # TODO: Need to support crypto?

  def run(self):
    self.running = True

    # Read in upload-only torrent files.
    for t in torrents:
      tinfo = SlyTorrentMetainfo(t)
      self.torrents_info[tinfo.get_infohash_raw()] = tinfo

    # Read in download torrents.
    for t in self.download_torrents:
      tinfo = SlyTorrentMetainfo(t)
      self.download_torrents_info[tinfo.get_infohash_raw()] = tinfo

    init_torrents()
    while True:
      time.sleep(UPDATE_INTERVAL_SEC)
      self.update_upload_download_bytes()

  def stop(self):
    for infohash, tinfo in self.torrents_info.iteritems():
      tinfo.stopped = True
    for infohash, tinfo in self.download_torrents_info.iteritems():
      tinfo.stopped = True
    with self.tracker_timers_lock:
      self.running = False
      for infohash, timer in self.tracker_timers.iteritems():
        timer.cancel()
    self.none_updating.acquire()
    while self.timers_updating:
      self.none_updating.wait()
    self.none_updating.release()
    for infohash, tinfo in self.torrents_info.iteritems():
      self.update_tracker(tinfo, event='stopped', force=True)
    for infohash, tinfo in self.download_torrents_info.iteritems():
      self.update_tracker(tinfo, event='stopped', force=True)

  def init_torrents(self):
    for infohash, tinfo in self.torrents_info.iteritems():
      self.torrent_locks[infohash] = threading.Lock()
      tinfo.bytes_up = 0
      tinfo.bytes_down = 0
      tinfo.bytes_left = 0
    for infohash, tinfo in self.download_torrents_info.iteritems():
      self.torrent_locks[infohash] = threading.Lock()
      tinfo.bytes_up = 0
      tinfo.bytes_down = 0
      tinfo.bytes_left = tinfo.get_size()
    for tinfo in (self.torrents_info.values() 
                  + self.download_torrents_info.values()):
      self.update_tracker(tinfo, event='started')

  def update_tracker(self, torrent_info, event=None, force=False):
    with self.timers_updating_lock:
      self.timers_updating += 1
    with self.torrent_locks[torrent_info.get_infohash_raw()]:
      if torrent_info.stopped and not force:
        return
      if event is None and torrent_info.just_completed:
        event = 'completed'
        torrent_info.just_completed = False
      response = tracker_utilquery_trackers(torrent_info, self.peer_id, 
                                            self.peer_port, 
                                            uploaded=torrent_info.bytes_up,
                                            downloaded=torrent_info.bytes_down,
                                            event=event,
                                            left=torrent_info.bytes_left,
                                            key=self.key)
    if response is None:
      pass # TODO: Decide what to do when update announce fails.
    else:
      with self.tracker_timers_lock:
        if self.running:
          t = threading.Timer(response['interval'], self.update_tracker, 
                              [tracker_url, torrent_info])
          torrent_info.tracker_timers[tracker_url] = t
          t.start()
    with self.timers_updating_lock:
      self.none_updating.acquire()
      self.timers_updating -= 1
      if self.timers_updating == 0:
        self.none_updating.notify()
      self.none_updating.release()

  def update_upload_download_bytes(self):
    remaining_up_bytes= int(random.uniform(0.99, 1.0) * 1024
                            * self.overall_upload_rate_kbps
                            * UPDATE_INTERVAL_SEC)
    remaining_down_bytes= int(random.uniform(0.99, 1.0) * 1024
                            * self.overall_download_rate_kbps
                            * UPDATE_INTERVAL_SEC)
    infohashes = self.download_torrents_info.keys()
    random.shuffle(infohashes)
    for infohash in infohashes:
      tinfo = self.download_torrents_info[infohash]
      # Move from download queue to upload queue if completed download.
      if tinfo.bytes_left == 0:
        del self.download_torrents_info[infohash]
        self.torrents_info[infohash] = tinfo
        tinfo.just_completed = True

      down_amt = int(random.uniform(0.97, 1.0) * 1024
                     * self.individual_download_rate_kbps * UPDATE_INTERVAL_SEC)
      down_amt = min(down_amt, tinfo.bytes_left, remaining_down_bytes)
      remaining_down_bytes -= down_amt
      with self.torrents_lock[infohash]:
        tinfo.bytes_down += down_amt
        tinfo.bytes_left -= down_amt

    tinfos = self.torrents_info.values()
    random.shuffle(tinfos)
    for tinfo in tinfos:
      up_amt = int(random.uniform(0.97, 1.0) * 1024 
                   * self.individual_upload_rate_kbps * UPDATE_INTERVAL_SEC)
      up_amt = min(up_amt, remaining_up_bytes)
      remaining_up_bytes -= up_amt
      tinfo.bytes_up += up_amt


class SlyTorrentMetainfo(torrentfile_util.TorrentMetainfo):
  def __init__(self, torrentfile):
    super(SlyTorrentMetainfo, self).__init__(torrentfile)
    self.bytes_up = 0
    self.bytes_down = 0
    self.bytes_left = 0
    self.just_completed = False
    self.stopped = False

if __name__ == '__main__':
  main()
