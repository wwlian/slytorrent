# Module providing functions for extracting metadata from torrent files

import copy
import hashlib
import random

from BitTornado.bencode import *

class TorrentMetainfo(object):
  def __init__(self, torrentfile):
    with open(torrentfile, 'rb') as infd:
      self.metainfo = bdecode(infd.read())
    self.announce_list = self.metainfo.get('announce-list')
    if self.announce_list is not None:
      for tier in self.announce_list:
        random.shuffle(tier)
    self.__infohash_obj = None

  @property
  def info(self):
    return copy.deepcopy(self.metainfo['info'])

  def get_piece_length(self):
    return self.info['piece length']

  def get_size(self):
    info = self.info
    if 'length' in info: # Single file.
      return info['length']
    else: # Multiple files.
      length_sum = 0
      for the_file in info['files']:
        length_sum += the_file['length']
      return length_sum

  def get_tracker(self):
    return self.metainfo['announce']

  def get_announce_list(self):
    """
      Returns a copy of the multitracker metadata info as a list of lists of 
      strings if present or None otherwise.
    """
    return copy.deepcopy(self.announce_list)

  def set_announce_list(self, announce_list):
    self.announce_list = copy.deepcopy(announce_list)

  def get_infohash_hex(self):
    """
    Returns the SHA1 hash of this object's info dictionary as
    a hexadecimal string.
    """
    if self.__infohash_obj is None:
      self.__infohash_obj = hashlib.sha1(bencode(self.info))
    return self.__infohash_obj.hexdigest()

  def get_infohash_raw(self):
    """
    Returns the SHA1 hash of this object's info dictionary as
    a byte string.
    """
    if self.__infohash_obj is None:
      self.__infohash_obj = hashlib.sha1(bencode(self.info))
    return self.__infohash_obj.digest()
