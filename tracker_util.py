# Module providing functions for talking to a BT tracker

import struct
import sys
import time
import urllib
import urllib2

from BitTornado.bencode import *

import torrentfile_util

def sample_peers(torrent_info, peer_id, peer_port, numwant=50, uploaded=0, 
                 downloaded=0, left=0, compact=True, event=None, key=None):
  """Queries a torrent's trackers for a set of peers in the swarm.

  Peers are collected from each of the trackers until numwant is met, or 
  all trackers are exhausted.
  If the tracker provides a compact peer list, the peer id field will be
  None for all peers.

  Args:
    torrent_info: A TorrentMetainfo object.
    peer_id: The peer ID to send with all tracker requests.
    peer_port: The listening port to report to the tracker.
    numwant: The maximum number of peers to return.
    uploaded: The number of uploaded bytes to report to the tracker.
    downloaded: The number of downloaded bytes to report to the tracker.
    left: The number of bytes remaining to be downloaded to report to the 
      tracker.
    compact: Whether or not to request a compact peers list. Since this flag
      is merely advisory, the returned peer list may or may not conform to
      the requested format.
    event: The event to be reported to the tracker. Leave as None to omit.
    key: A client-identification key to send to the tracker.

  Returns:
    A set of up to numwant peers as (peer id, IP/host, port) tuples of type 
      (str, str, int).
  """
  announce_list = torrent_info.get_announce_list()
  if announce_list is None:
    announce_list = [[torrent_info.get_tracker()]]
  infohash = torrent_info.get_infohash_raw()

  params = {'info_hash': infohash, 
            'peer_id': peer_id, 
            'port': peer_port, 
            'numwant': numwant,
            'uploaded': uploaded, 
            'downloaded': downloaded,
            'left': left}
  if event is not None:
    params['event'] = event
  if key is not None:
    params['key'] = key
  params = urllib.urlencode(params)

  peer_set = set()
  for tracker_tier in announce_list:
    for tracker_url in tracker_tier:
      if len(peer_set) < numwant:
        f = urllib2.urlopen("%s?%s" % (tracker_url, params))
        resp = bdecode(f.read())
        if 'failure reason' not in resp:
          peers = resp['peers']
          if isinstance(peers, list):
            peer_set = peer_set.union([(peer['id'], peer['ip'], peer['port']) 
                                       for peer in peers])
          elif isinstance(peers, str): # Compact peer list.
            peer_set = peer_set.union([(None, p[0], p[1])
                                       for p in decode_compact_peers(peers)])
          else:
            raise RuntimeError('Tracker returned unexpected peer list type: '
                               '%s' % type(peers).__name__)

  while peer_set and len(peer_set) > numwant:
    peer_set.pop()
  return peer_set

def query_trackers(torrent_info, peer_id, peer_port, numwant=50, uploaded=0,
                   downloaded=0, left=0, compact=True, event=None, key=None):
  """Announces a client's participation in a swarm.

  Args:
    torrent_info: A TorrentMetainfo object.
    peer_id: The peer ID to send with all tracker requests.
    peer_port: The listening port to report to the tracker.
    numwant: The maximum number of peers for each tracker to return.
    uploaded: The number of uploaded bytes to report to the tracker.
    downloaded: The number of downloaded bytes to report to the tracker.
    left: The number of bytes remaining to be downloaded to report to the 
      tracker.
    compact: Whether or not to request a compact peers list. Since this flag
      is merely advisory, the returned peer list may or may not conform to
      the requested format.
    event: The event to be reported to the tracker. Leave as None to omit.
    key: A client-identification key to send to the tracker.

  Returns:
    The decoded response of the first reachable tracker.
  """
  announce_list = torrent_info.get_announce_list()
  if announce_list is None:
    announce_list = [[torrent_info.get_tracker()]]
  new_announce_list = [list(tier) for tier in announce_list]

  for i in xrange(len(announce_list)):
    tier = announce_list[i]
    for j in xrange(len(tier)):
      tracker_url = tier[j]
      response = query_tracker(torrent_info, tracker_url, peer_id, peer_port,
                               numwant, uploaded, downloaded, left, compact, 
                               event, key)
      if response is None:
        continue
      else:
        # Promote tracker to front of tier on success per BEP 12.
        # http://bittorrent.org/beps/bep_0012.html#order-of-processing
        new_announce_list[i].remove(j)
        new_announce_list[i].insert(0, tracker_url)
        break

  torrent_info.set_announce_list(new_announce_list)
  return response

def query_tracker(torrent_info, tracker_url, peer_id, peer_port, numwant=50, uploaded=0,
                  downloaded=0, left=0, compact=True, event=None, key=None):
  """Announces a client's participation in a swarm to a single tracker.

  Args:
    torrent_info: A TorrentMetainfo object.
    tracker_url: The URL of the tracker to query.
    peer_id: The peer ID to send with all tracker requests.
    peer_port: The listening port to report to the tracker.
    numwant: The maximum number of peers for each tracker to return.
    uploaded: The number of uploaded bytes to report to the tracker.
    downloaded: The number of downloaded bytes to report to the tracker.
    left: The number of bytes remaining to be downloaded to report to the 
      tracker.
    compact: Whether or not to request a compact peers list. Since this flag
      is merely advisory, the returned peer list may or may not conform to
      the requested format.
    event: The event to be reported to the tracker. Leave as None to omit.
    key: A client-identification key to send to the tracker.

  Returns:
    The tracker's decoded response or None if the query failed.
  """
  infohash = torrent_info.get_infohash_raw()

  params = {'info_hash': infohash, 
            'peer_id': peer_id, 
            'port': peer_port, 
            'numwant': numwant,
            'uploaded': uploaded, 
            'downloaded': downloaded,
            'left': left,
            'compact': 1 if compact else 0}
  if event is not None:
    params['event'] = event
  if key is not None:
    params['key'] = key
  params = urllib.urlencode(params)
  
  try:
    f = urllib2.urlopen("%s?%s" % (tracker_url, params))
  except urllib2.URLError, err:
    return None
  except:
    return None

  resp = bdecode(f.read())
  if 'failure reason' in resp:
    return None
  else:
    return resp

def decode_compact_peers(peers_bytes):
  """Decodes the compact representation of swarm peers.

  The input to this function can be obtained by querying the tracker for peers
  with the compact flag set.

  Args:
    peers_bytes: A byte string containing the compact representation of a list
      of peers. Length should be an integer multiple of 6.

  Returns:
    A list of (IP, port) tuples of type (str, int).
  """
  if not isinstance(peers_bytes, str):
    raise ValueError('peers_bytes must be a byte string of length a multiple '
                      'of 6. Invalid type.')
  elif len(peers_bytes) % 6:
    raise ValueError('peers_bytes must be a byte string of length a multiple '
                      'of 6. Invalid length.')
  results = []
  for i in xrange(0, len(peers_bytes), 6):
    results.append(decode_compact_peer(peers_bytes[i:i + 6]))
  return results

def decode_compact_peer(peer_bytes):
  """Decodes the compact representation of a swarm peer.

  The input to this function can be obtained by querying the tracker for peers
  with the compact flag set and extracting a substring of length 6 aligned
  to 6-byte boundaries.

  Args:
    peer_bytes: A byte string of length 5 containing the compact 
      representation of a peer.

  Returns:
    An (IP, port) tuple of type (str, int).
  """
  if not isinstance(peer_bytes, str):
    raise ValueError('peer_bytes must be a byte string of length 6. Invalid type.')
  elif len(peer_bytes) != 6:
    raise ValueError('peer_bytes must be a byte string of length 6. Invalid length.')
  ip_str = '.'.join(map(str, struct.unpack('BBBB', peer_bytes[:4])))
  port_num = struct.unpack('!H', peer_bytes[-2:])[0]
  return ip_str, port_num
